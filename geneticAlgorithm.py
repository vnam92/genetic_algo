#!/usr/bin/env python3

import random
from operator import itemgetter


class GeneticAlgorithm(object):
    def __init__(self, genetics):
        self.genetics = genetics
        pass

    def run(self):
        """
        Механика работы метода  :
        инициализация случайной популяции(t)
        определение приспособленности популяции(t)
        цикл
            выбор родителей из популяции(t)
            селекция(отбор) на родителях создающих популяцию(t+1)
            мутация популяции(t+1)
            определение приспособленности популяции(t+1)
            пока не получена лучшая популяция
        :return:
        """
        population = self.genetics.get_init_population()
        while True:
            # вычисление приспособленности хромосом в популяции
            fitness_population = [(self.genetics.fitness(chromosome), chromosome) for chromosome in population]
            if self.genetics.check_stop(fitness_population):
                self.genetics.render_result()
                break
            population = self.get_next_population(fitness_population)
        return population

    def get_next_population(self, population):
        parents_generator = self.genetics.parents(population)
        size = len(population)
        next_pop_l = []
        save_best = self.genetics.save_best

        # сохранять лучшее из предыдущей популяции
        sorted_fits = sorted(population, key=itemgetter(0))
        for i in range(1, save_best + 1):
            next_pop_l.append(sorted_fits[-i][1])

        while len(next_pop_l) < size:
            parents = next(parents_generator)
            cross = random.random() < self.genetics.get_probability_crossover()
            children = self.genetics.crossover(parents) if cross else parents
            if random.random() < self.genetics.get_probability_mutation():
                for ch in children:
                    mutate = random.random() < self.genetics.get_probability_mutation()
                    next_pop_l.append(self.genetics.mutation(ch) if mutate else ch)
                    pass
                pass
            pass
        return next_pop_l[0:size]
    pass
