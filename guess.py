#!/usr/bin/env python3

import random
from matplotlib import pyplot as plt


class Guess:
    def __init__(self, target_text, limit, size, probability_crossover, probability_mutation, save_best):
        self.counter = 0
        self.fitness_best = []
        self.fitness_worst = []
        self.fitness_average = []

        self.target = self.text_to_chromo(target_text)
        self.limit = limit
        self.size = size
        self.probability_crossover = probability_crossover
        self.probability_mutation = probability_mutation
        self.save_best = save_best

    def get_probability_crossover(self):
        return self.probability_crossover

    def get_probability_mutation(self):
        return self.probability_mutation

    def get_init_population(self):
        return [self.random_chromo() for j in range(self.size)]

    def fitness(self, chromo):
        # larger is better, matched == 0
        return -sum(abs(c - t) for c, t in zip(chromo, self.target))

    def check_stop(self, fits_populations):
        self.counter += 1

        best_match = list(sorted(fits_populations))[-1]
        best_fitness = best_match[0]
        best_individual = best_match[1]

        fits = [f for f, ch in fits_populations]

        best = max(fits)
        worst = min(fits)
        average = sum(fits) / len(fits)

        if self.counter % 1 == 0:

            self.fitness_best.append((self.counter, best))
            self.fitness_worst.append((self.counter, worst))
            self.fitness_average.append((self.counter, average))

            if self.counter % 10 == 0:
                print("[%3d] Приспособленность=(Л: %4d, Ср: %4d, Х: %4d): %r" % (
                    self.counter, best, average, worst, self.chromo_to_text(best_individual)))

        if best_fitness == 0:
            print("[%3d] Приспособленность=(Л: %4d, Ср: %4d, Х: %4d): %r" % (
                self.counter, best, average, worst, self.chromo_to_text(best_individual)))
            return True
        return self.counter >= self.limit

    def parents(self, fits_populations):
        while True:
            best_1 = self.tournament(fits_populations)
            best_2 = self.tournament(fits_populations)
            yield best_1, best_2
            pass
        pass

    def tournament(self, fits_populations):
        rand_1_fitness, rand_1 = self.select_random(fits_populations)
        rand_2_fitness, rand_2 = self.select_random(fits_populations)
        if rand_1_fitness > rand_2_fitness:
            return rand_1
        else:
            return rand_2

    def crossover(self, parents):
        parent1, parent2 = parents

        index1 = random.randint(1, len(self.target) - 2)
        index2 = random.randint(1, len(self.target) - 2)

        if index1 > index2:
            index1, index2 = index2, index1

        child1 = parent1[:index1] + parent2[index1:index2] + parent1[index2:]
        child2 = parent2[:index1] + parent1[index1:index2] + parent2[index2:]

        return child1, child2

    def mutation(self, chromosome):
        index = random.randint(0, len(self.target) - 1)
        vary = random.randint(-5, 5)
        mutated = list(chromosome)
        mutated[index] += vary
        return mutated

    @staticmethod
    def select_random(fits_populations):
        return fits_populations[random.randint(0, len(fits_populations) - 1)]

    @staticmethod
    def text_to_chromo(text):
        return [ord(ch) for ch in text]

    @staticmethod
    def chromo_to_text(chromo):
        return "".join(chr(max(1, min(ch, 1300))) for ch in chromo)

    def random_chromo(self):
        return [random.randint(1, 1024) for i in range(len(self.target))]

    def render_result(self):
        points = [f for f, ch in self.fitness_best]
        fitness_best = [seq[1] for seq in self.fitness_best]
        fitness_worst = [seq[1] for seq in self.fitness_worst]
        fitness_average = [seq[1] for seq in self.fitness_average]

        plt.plot(points, fitness_best, color='green', label='Лучшая приспособленность')
        plt.plot(points, fitness_average, linestyle='dashdot', color='purple', label='Средняя приспособленность')
        plt.plot(points, fitness_worst, linestyle='dotted', color='red', label='Худшая приспособленность')

        plt.xlabel("Популяция")
        plt.ylabel("Расстояние от оптимальных значений параметров ГА")

        plt.legend()
        plt.show()
        pass
    pass
