from geneticAlgorithm import GeneticAlgorithm
from guess import Guess

if __name__ == "__main__":
    target_text = "Мир"

    limit = 1000            # кол-во поколений (длительность эволюции)
    size = 400              # размер популяции
    prob_crossover = 0.9    # вероятность кросинговера
    prob_mutation = 0.1     # вероятность мутации
    save_best = 5           # сохранять лучших потомков из популяции

    GeneticAlgorithm(Guess(target_text, limit, size, prob_crossover, prob_mutation, save_best)).run()
